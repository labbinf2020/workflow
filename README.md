**Workflow**

- This is the main repository that you need in order to run the snakefile and make your analysis.
- It has all the directorys, the snake file, the config.json and all the scripts needed.


**Snakefile**

- Consists in a python script that connects all the needed scripts from the beggining to the last part before the analysis.
- It has a set of rules that need input and output files, and they are all connected.

**Config.json**

- Here you can set all the parameters you want to change for the rules, by changing the values from the keys.
- ATENTION:Don't change the keys names in the file!!!
-It's here where you set you percentage to whatever you want



**How to run**

- Activate the conda envoirnment: conda activate snakemake
- Then, you need to move your fasta file to here -> "data/D01/input"    
-Change your directory for the main Workflow file (ex: cd Workflow)


- And in the terminal, write the following code and run it:


_OUTPUTDIR="data/D01/output" DATASET="data/D01/input/your_sequence.fasta" snakemake "the name of the rules you want to run" --cores all_


For the rules you want to run depends on what you want to obtain:
-If you just want the trees (original and/or missing data), you can choose this commands (if you want all the trees type all the commands):

    Create_Bayesian_Trees
    Create_Bayesian_Trees_For_Randomize_Any


    Create_MaximumLikelyhood_Trees
    Create_MaximumLikelyhood_Trees_For_Randomize_Any


-If you want to run the iqTree Analysis for ML or BI Trees you type:

    IqTree_Analysis_ML
    IqTree_Analysis_BI

-If you want to run the iqTree Analysis for ETE Tools you type:

    Ete_tools
    Ete_tools_Any

Here is an example of a command if you just want to produce Bayesian Trees for **10%** missing data and make the IqTree analysis for **10%**:

-First you open config.json and set the value from the percentage key, to **10** from missing data you want 

-Then you run the following code:

_OUTPUTDIR="data/D01/output" DATASET="data/D01/input/your_sequence.fasta" snakemake Create_Bayesian_Trees_For_Randomize_Any IqTree_Analysis_ML_Any --cores all_




You can use **-n** in snakemake to make a dry run, with that you can see the rules you are going to run and it's a good habit to have before running the file at all.








