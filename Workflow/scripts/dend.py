
import dendropy
import sys

post_trees = dendropy.TreeList()
post_trees.read(
         file=open(sys.argv[1], "r"),
         schema="nexus",
         tree_offset=1)
post_trees.read(
         path=sys.argv[2],
         schema="nexus",
         tree_offset=1)
post_trees.write(
     path=sys.argv[3],
     schema="newick")
